# @thredis

### Multi-Threaded Architecture with a Redis Shared Space and Flask web endpoints

## Tags

`service`, `python`, `flask`, `redis`, `rest`, `software architecture`

## Introduction

In this project, an architecture that allows to connect several applications together through a shared Redis space is presented. The project provides basic classes and decorators for creating a multi-threaded application with a shared Redis space and functions to exchange requests between them. In addition, the project provides a base class that allows you to create web endpoints elegantly using Flask for Python. This architecture can be used to develop distributed systems in which the frontend makes requests to the main or primary applications. As an example of the case, an app is made, consisting of a scrapper, imitating news scrapping, backend, receiving this news from the scrapper and processing them and the frontend, capable of sending requests to the backend and getting the number of processed news via desktop or GET requests.
## Getting Started

These instructions allow you to reproduce the project and run it on your local machine for development and testing purposes. 

### Prerequisites

The following software was used in this project:

* PyCharm: Python IDE for Professional Developers by JetBrains;
* Anaconda 4.4.0 Python 2.7 version;
* Redis 4.0.5 version;
* Python modules can be installed by `pip install`.

### Project structure

    ├── data                                    # data files
        ├── ...
    ├── src                                     # project source
        ├── application                         # basic classes and decorators together with application implementation
            ├── ...
        ├── modules                             # additional modules of a common purpose
            ├── ...
    ├── config.py
    ├── main.py
    ├── ...    

`/src` has to be marked in PyCharm as sources root folder.

### Glossary

* [Redis](https://redis.io) – an open source (BSD licensed), in-memory data structure store, used as a database, cache and message broker. It supports data structures such as strings, hashes, lists, sets, sorted sets with range queries, bitmaps, hyperloglogs and geospatial indexes with radius queries. Redis has built-in replication, Lua scripting, LRU eviction, transactions and different levels of on-disk persistence, and provides high availability via Redis Sentinel and automatic partitioning with Redis Cluster. 

## More Detail

### Multi-Threaded Application

`/src/application/core.py` provides `BaseApplication` class which inherited allows to make multithreaded applications:

```
app._add_task(handler, delay)  # adds a function to threading tasks
app.run_polling()  # runs the application polling starting all the threads
```

`@thread` decorator from `/src/application/decorators.py` has to be used on the functions marking them as threads.

### Redis Application

`/src/application/core.py` provides also `SenderApplication` and `ReceiverApplication` classes which are used in order to add applications the ability to communicate among themselves through requests:

<p align="center">
  <img src="/uploads/a777a0b897d6f1b7cd9cd67d39cb30e5/scheme.png" width="600px" >
</p>


#### Usage

* `SenderApplication` object side:

```
request = self._new_request(some_function.__name__, args=args, kwargs=kwargs)
index = self._push_redis_request(request, "backend")
answer = self._pull_redis_answer(-index, "backend")  # wait
```

* `ReceiverApplication` object side has to contain `some_function`.

### Flask Application

`/src/application/core.py` provides `FlaskApplication` class which inherited allows to create applications with web endpoints:

```
app._add_route("/endpoint_path", handler)  # adds an endpoint
app.run_polling()  # runs flask application
```

## Running the project

The main file is `main.py` located in the root folder.

<p align="center">
  <img src="/uploads/ed388f003869d11abb6fe93efb2291f1/example.png" width="600px" >
</p>

The show-case contains a scrapper that will simulate finding news feeds every 10 seconds and sending them to the main application, i.e. backend, via Redis. The real-time backend receives news feeds from the scrapper and processes them every 30 seconds, updating the number of processed news in the local variable. In turn, the frontend allows you to send requests to the backend via desktop command or web endpoint asking the number of processed news and receiving the response.

```
INFO:application.core:starting thread __perform_requests()..
INFO:application.core:starting thread __listen_requests()..
INFO:application.core:starting thread __listen_registers()..
INFO:application.scrapper:starting thread scrap_feeds()..
INFO:application.core:starting thread __perform_requests()..
INFO:application.core:starting thread __listen_requests()..
INFO:application.core:starting thread __listen_registers()..
INFO:application.backend:starting thread process_feeds()..
INFO:application.backend:starting thread listen_feeds()..
INFO:application.frontend:starting thread send_requests()..
INFO:application.core:..adding /feeds_count endpoint
INFO:werkzeug: * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
INFO:application.scrapper:..new feeds scrapped: 3
INFO:application.backend:..new feeds found: 3

Perform request? (y/n) y
INFO:application.backend:call to requestable function get_feeds_count()..
..got the answer: 0

INFO:application.backend:param: 1
INFO:application.scrapper:..new feeds scrapped: 1
INFO:application.backend:..new feeds found: 1
INFO:application.backend:processing feeds: 4..
INFO:application.scrapper:..new feeds scrapped: 1
INFO:application.backend:..new feeds found: 1
INFO:application.scrapper:..new feeds scrapped: 2
INFO:application.backend:..new feeds found: 2

Perform request? (y/n) y
INFO:application.backend:call to requestable function get_feeds_count()..
..got the answer: 4
```

Calling a function using a web request with some parameter for example:

<p align="center">
  <img src="/uploads/6677fe61eefa126730a4ea36cce776af/thread_request.png" width="600px" >
</p>
