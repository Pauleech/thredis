import uuid
import config
import numpy as np
from decorators import thread, redisop
from core import ReceiverApplication
from modules.logering import setup_logger
logger = setup_logger(__name__, config.log_name, config.log_level)


class Scrapper(ReceiverApplication):
    def __init__(self, redis_params):
        # type: (dict) -> None
        """
        Initiates Scrapper object.

        :rtype: None
        :param redis_params: redis shared space parameters dictionary.
        :return: None.
        """
        ReceiverApplication.__init__(self, redis_params, "scrapper")
        # functionality
        self.feeds_field = "feeds"
        # threading
        self._add_task(self.scrap_feeds, 10)

    @redisop(logger)
    def push_redis_feeds(self, feeds):
        # type: (list) -> None
        """
        Pushes feeds to the redis feeds queue.

        :rtype: None
        :param feeds: feeds.
        :return: None.
        """
        self._redis.lpush(self.feeds_field, self._pack_data(feeds))

    @thread(logger)
    def scrap_feeds(self):
        # type: () -> None
        """
        Imitates scrapper functionality creating feeds with some time delay.

        :rtype: None
        :return: None.
        """
        try:
            feeds_data = [uuid.uuid4() for _ in range(np.random.randint(0, 5))]
            logger.info("..new feeds scrapped: {}".format(len(feeds_data)))
            if len(feeds_data) != 0:
                self.push_redis_feeds(feeds_data)
        except Exception as e:
            logger.error("..error in scrap_feeds thread: {}".format(e))
