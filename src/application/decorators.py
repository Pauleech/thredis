import functools


def requestable(logger):
    """
    A decorator which can be used to mark functions as proposed for requestable usage.
    """

    def outer(func):
        @functools.wraps(func)
        def wrapped(*args, **kwargs):
            logger.info("call to requestable function {}()..".format(func.__name__))
            return func(*args, **kwargs)

        return wrapped

    return outer


def redisop(logger):
    """
    A decorator which can be used to mark functions as belonging to redis operations.
    """

    def outer(func):
        @functools.wraps(func)
        def wrapped(*args, **kwargs):
            return func(*args, **kwargs)

        return wrapped

    return outer


def thread(logger):
    """
    A decorator which can be used to mark functions as polling threads.
    """

    def outer(func):
        @functools.wraps(func)
        def wrapped(self, *args, **kwargs):
            logger.info("starting thread {}()..".format(func.__name__))
            while not self._stop_event.wait(timeout=self._thread_delays[func.__name__]):
                func(self, *args, **kwargs)
            logger.info("..thread {}() stopped".format(func.__name__))

        return wrapped

    return outer
