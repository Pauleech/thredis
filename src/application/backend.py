import config
from Queue import Queue
from core import ReceiverApplication
from modules.logering import setup_logger
from decorators import thread, redisop, requestable
logger = setup_logger(__name__, config.log_name, config.log_level)


class Backend(ReceiverApplication):
    def __init__(self, redis_params):
        # type: (dict) -> None
        """
        Initiates Backend object.

        :rtype: None
        :param redis_params: redis shared space parameters dictionary.
        :return: None.
        """
        ReceiverApplication.__init__(self, redis_params, "backend")
        # functionality
        self.feeds_queue = Queue()
        self.feeds_field = "feeds"
        self.feeds_count = 0
        # threading
        self._add_task(self.listen_feeds, 0.1)
        self._add_task(self.process_feeds, 30)

    @requestable(logger)
    def get_feeds_count(self, param=0):
        # type: () -> int
        """
        Returns a feeds count.

        :rtype: int
        :return: feeds count.
        """
        logger.info("param: {}".format(param))
        return self.feeds_count

    @redisop(logger)
    def pull_redis_feeds(self):
        # type: () -> None
        """
        Pulls redis feeds and puts them to the feeds queue.

        :rtype: None
        :return: redis request.
        """
        pulled = self._redis.rpop(self.feeds_field)
        if pulled is not None:
            feeds = self._unpack_data(pulled)  # type: list
            self.feeds_queue.put(feeds)
            logger.info("..new feeds found: {}".format(len(feeds)))

    @thread(logger)
    def listen_feeds(self):
        # type: () -> None
        """
        Listens for the new feeds in redis.

        :rtype: None
        :return: None.
        """
        try:
            self.pull_redis_feeds()
        except Exception as e:
            logger.error("..error in listen_feeds thread: {}".format(e))

    @thread(logger)
    def process_feeds(self):
        # type: () -> None
        """
        Processes new feeds are received.

        :rtype: None
        :return: None.
        """
        try:
            feeds = []
            while not self.feeds_queue.empty():
                feeds.append(self.feeds_queue.get())
            feeds = [item for sublist in feeds for item in sublist]
            self.feeds_count += len(feeds)
            logger.info("processing feeds: {}..".format(len(feeds)))
        except Exception as e:
            logger.error("..error in process_feeds thread: {}".format(e))
