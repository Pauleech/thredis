import config
from decorators import thread
from modules.logering import setup_logger
from core import BaseApplication, SenderApplication, FlaskApplication
logger = setup_logger(__name__, config.log_name, config.log_level)


class DesktopFrontend(BaseApplication, SenderApplication):
    def __init__(self, redis_params):
        # type: (dict) -> None
        """
        Initiates DesktopFrontend object.

        :rtype: None
        :param redis_params: redis shared space parameters dictionary.
        :return: None.
        """
        BaseApplication.__init__(self)
        SenderApplication.__init__(self, redis_params, "desktop")
        # threading
        self._add_task(self.send_requests, 0.01)

    @thread(logger)
    def send_requests(self):
        # type: () -> None
        """
        Imitates frontend functionality sending requests.

        :rtype: None
        :return: None.
        """
        try:
            perform = raw_input("Perform request? (y/n) ")
            if perform == "y":
                request = self._new_request("get_feeds_count", param=1)
                index = self._push_redis_request(request, "backend")
                answer = self._pull_redis_answer(-index, "backend")  # wait
                print "..got the answer: {}".format(answer)
        except Exception as e:
            logger.error("..error in send_requests thread: {}".format(e))


class EndpointsFrontend(FlaskApplication, SenderApplication):
    def __init__(self, redis_params):
        # type: (dict) -> None
        """
        Initiates EndpointsFrontend object.

        :rtype: None
        :param redis_params: redis shared space parameters dictionary.
        :return: None.
        """
        FlaskApplication.__init__(self, "endpoints")
        SenderApplication.__init__(self, redis_params, "endpoints")
        # threading
        self._add_route("/feeds_count", self.request_receiver, "get_feeds_count")

    def request_receiver(self, request_args, handler_name):
        # type: (dict, str, str) -> object
        """
        Sends Redis request to receiver.

        :rtype: object
        :param request_args: rest arguments.
        :param handler_name: request function name.
        :return: receiver answer.
        """
        request_args = {k: v for k, v in request_args.items()}
        request = self._new_request(handler_name, **request_args)
        index = self._push_redis_request(request, "backend")
        answer = self._pull_redis_answer(-index, "backend")  # wait
        logger.info("..answer received: {}".format(type(answer)))
        return self._json_data(answer)
