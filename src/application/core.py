import json
import redis
import flask
import config
import cPickle as pickle
from Queue import Queue
from flask import Flask
from threading import Thread, Event
from decorators import redisop, thread
from modules.logering import setup_logger
logger = setup_logger(__name__, config.log_name, config.log_level)


class BaseApplication(object):
    def __init__(self):
        # type: () -> None
        """
        Initiates BaseApplication object.

        :rtype: None
        :return: None.
        """
        # threading
        self.__tasks = dict()
        self.__tasks_args = dict()
        self.__tasks_kwargs = dict()
        self.__threads = []
        self._stop_event = Event()
        self._thread_delays = dict()

    def _add_task(self, handler, thread_delay, *args, **kwargs):
        # type: (function, float) -> None
        """
        Adds a function to threading tasks.

        :rtype: None
        :param handler: a function to be launched in a thread.
        :param thread_delay: thread delay.
        :return: None.
        """
        self.__tasks.update({handler.__name__: handler})
        self.__tasks_args.update({handler.__name__: args})
        self.__tasks_kwargs.update({handler.__name__: kwargs})
        self._thread_delays.update({handler.__name__: thread_delay})

    def __start_threads(self):
        # type: () -> None
        """
        Launches all threads.

        :rtype: None
        :return: None.
        """
        self.__threads = [Thread(target=v, args=self.__tasks_args[k],
                                 kwargs=self.__tasks_kwargs[k], name=k)
                          for k, v in self.__tasks.iteritems()]
        self._stop_event.clear()
        map(Thread.start, self.__threads)

    def stop_threads(self):
        # type: () -> None
        """
        Stops all threads.

        :rtype: None
        :return: None.
        """
        self._stop_event.set()
        map(Thread.join, self.__threads)

    def run_polling(self):
        # type: () -> None
        """
        Runs the application polling.

        :rtype: None
        :return: None.
        """
        self.__start_threads()


class RedisApplication(object):
    def __init__(self, redis_params):
        # type: (dict) -> None
        """
        Initiates RedisApplication object.

        :rtype: None
        :param redis_params: redis shared space parameters dictionary.
        :return: None.
        """
        # shared space
        self._redis = redis.StrictRedis(**redis_params)
        self._registers_field = "registers"
        self._senders_field = "senders"

    @staticmethod
    def _pack_data(data):
        # type: (object) -> str
        """
        Returns packed data for redis communication.

        :rtype: str
        :return: packed data.
        """
        return pickle.dumps(data)

    @staticmethod
    def _unpack_data(data):
        # type: (str) -> object
        """
        Returns unpacked data for redis communication.

        :rtype: object
        :return: unpacked data.
        """
        return pickle.loads(data)


class ReceiverApplication(BaseApplication, RedisApplication):
    def __init__(self, redis_params, name):
        # type: (dict, str) -> None
        """
        Initiates ReceiverApplication object.

        :rtype: None
        :param redis_params: redis shared space parameters dictionary.
        :param name: universally unique identifier.
        :return: None.
        """
        BaseApplication.__init__(self)
        RedisApplication.__init__(self, redis_params)
        # fields
        self.__name = name
        self.__requests_queue = Queue()
        self.__requests_fields = self.__get_requests_fields()
        self.__clear_requests()
        # threading
        self._add_task(self.__listen_requests, 0.01)
        self._add_task(self.__listen_registers, 0.01)
        self._add_task(self.__perform_requests, 0.01)

    @redisop(logger)
    def __get_requests_fields(self):
        # type: () -> list
        """
        Fetches and returns list of frontend members redis request fields.

        :rtype: list
        :return: list of frontend members redis request fields.
        """
        members = self._redis.smembers(self._senders_field)
        return ["requests-{}-{}".format(member, self.__name) for member in members]

    @redisop(logger)
    def __clear_requests(self):
        # type: () -> None
        """
        Deletes requests fields in redis for current backend.

        :rtype: None
        :return: None.
        """
        self._redis.delete(self.__get_requests_fields())

    @redisop(logger)
    def __pull_redis_registers(self):
        # type: () -> None
        """
        Pulls redis registers and updates requests fields.

        :rtype: None
        :return: None.
        """
        pulled = self._redis.rpop(self._registers_field)
        if pulled is not None:
            self.__requests_fields = self.__get_requests_fields()

    @redisop(logger)
    def __pull_redis_requests(self):
        # type: () -> None
        """
        Pulls redis requests and puts them to the requests queue.

        :rtype: None
        :return: None.
        """
        for requests_field in self.__requests_fields:
            pulled = self._redis.rpop(requests_field)
            if pulled is not None:
                request = self._unpack_data(pulled)
                self.__requests_queue.put(request)

    @redisop(logger)
    def __push_redis_answer(self, request):
        # type: (dict) -> None
        """
        Pushes redis answer.

        :rtype: None
        :param request: request dictionary.
        :return: None.
        """
        answer = request["answer"]
        answers_field = "answers-{}-{}".format(request["name"], self.__name)
        self._redis.lpush(answers_field, self._pack_data(answer))

    @thread(logger)
    def __listen_registers(self):
        # type: () -> None
        """
        Listens for the new register.

        :rtype: None
        :return: None.
        """
        try:
            self.__pull_redis_registers()
        except Exception as e:
            logger.error("..error in listen_requests thread: {}".format(e))

    @thread(logger)
    def __listen_requests(self):
        # type: () -> None
        """
        Listens for the redis requests.

        :rtype: None
        :return: None.
        """
        try:
            self.__pull_redis_requests()
        except Exception as e:
            logger.error("..error in listen_requests thread: {}".format(e))

    @thread(logger)
    def __perform_requests(self):
        # type: () -> None
        """
        Performs the redis requests.

        :rtype: None
        :return: None.
        """
        try:
            request = self.__requests_queue.get()
            try:
                request["answer"] = getattr(self, request["request"])(*request["args"], **request["kwargs"])
            except Exception as e:
                logger.error("..cannot perform request: {}".format(e))
            self.__push_redis_answer(request)
        except Exception as e:
            logger.error("..error in perform_requests thread: {}".format(e))


class SenderApplication(RedisApplication):
    def __init__(self, redis_params, name, request_timeout=5):
        # type: (dict, str, int) -> None
        """
        Initiates SenderApplication object.

        :rtype: None
        :param redis_params: redis shared space parameters dictionary.
        :param name: universally unique identifier.
        :param request_timeout: timeout for the request answer; if timeout is 0, then block indefinitely.
        :return: None.
        """
        RedisApplication.__init__(self, redis_params)
        self.__name = name
        self.__request_timeout = request_timeout
        self.__requests_field = "requests-{}".format(self.__name)
        self.__answers_field = "answers-{}".format(self.__name)
        self.__register()
        self.__clear_answers()

    @redisop(logger)
    def __register(self):
        # type: () -> None
        """
        Registers current FrontendApplication in redis.

        :rtype: None
        :return: None.
        """
        self._redis.sadd(self._senders_field, self.__name)
        self._redis.lpush(self._registers_field, self._pack_data(True))

    @redisop(logger)
    def __clear_answers(self):
        # type: () -> None
        """
        Deletes answers field in redis for current frontend.

        :rtype: None
        :return: None.
        """
        self._redis.delete(self.__answers_field)

    def _new_request(self, handler_name, *args, **kwargs):
        # type: (str) -> dict
        """
        Returns new request dictionary.

        :rtype: dict
        :param handler_name: request function name.
        :return: request dictionary.
        """
        return {"name": self.__name, "request": handler_name, "args": args, "kwargs": kwargs, "answer": None}

    @redisop(logger)
    def _push_redis_request(self, request, destination):
        # type: (dict, str) -> int
        """
        Pushes redis request appending given request to the requests queue.

        :rtype: int
        :param request: a request.
        :param destination: destination backend.
        :return: number of requests in a queue.
        """
        requests_field = "{}-{}".format(self.__requests_field, destination)
        return self._redis.lpush(requests_field, self._pack_data(request))

    @redisop(logger)
    def _pull_redis_answer(self, index, source):
        # type: (int, str) -> object
        """
        Returns redis request answer.
        Deletes the request from the queue if it has not been executed.

        :rtype: object
        :param: source backend.
        :return: request answer.
        """
        output = None
        answers_field = "{}-{}".format(self.__answers_field, source)
        requests_field = "{}-{}".format(self.__requests_field, source)
        pulled = self._redis.brpop(answers_field, timeout=self.__request_timeout)
        if pulled is None:
            logger.warn("..timeout: no answer!")
            p = self._redis.pipeline()
            p.lset(requests_field, index, "to_delete")
            p.lrem(requests_field, 1, "to_delete")
            p.execute()
        else:
            output = self._unpack_data(pulled[1])
        return output


class FlaskApplication(object):
    def __init__(self, name):
        # type: (str) -> None
        """
        Initiates FlaskApplication object.

        :rtype: None
        :param name: universally unique identifier.
        :return: None.
        """
        self.__flask = Flask(name)

    @staticmethod
    def _json_data(data):
        # type: (object) -> str
        """
        Returns json data for rest communication.

        :rtype: str
        :return: packed data.
        """
        return json.dumps(data)

    def _add_route(self, endpoint, handler, *args, **kwargs):
        # type: (str, handler) -> None
        """
        Adds a route.

        :rtype: None
        :param endpoint: endpoint name.
        :param handler: request function.
        :return: None.
        """
        logger.info("..adding {} endpoint".format(endpoint))

        @self.__flask.route(endpoint, endpoint=endpoint)
        def route():
            return handler(flask.request.args, *args, **kwargs)

    def run_polling(self):
        # type: () -> None
        """
        Runs the application polling.

        :rtype: None
        :return: None.
        """
        self.__flask.run()
