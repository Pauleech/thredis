import config
from application.backend import Backend
from application.scrapper import Scrapper
from application.frontend import DesktopFrontend, EndpointsFrontend


def main():
    """
    Module main function.
    """
    app = Scrapper(config.redis_params)
    app.run_polling()
    app = Backend(config.redis_params)
    app.run_polling()
    app = DesktopFrontend(config.redis_params)
    app.run_polling()
    app = EndpointsFrontend(config.redis_params)
    app.run_polling()


if __name__ == "__main__":
    main()
