import os
import logging
import platform
import multiprocessing
from modules.logering import setup_logger

# number of cores
cores = multiprocessing.cpu_count()

# operating system
system = platform.system()

# folders
root_path = os.path.dirname(os.path.realpath(__file__))
data_path = os.path.join(root_path, "data")

# seed
seed = 42

# logging
log_level = logging.INFO
log_name = os.path.join(root_path, "thredis.log")
logger = setup_logger("logger", log_name, log_level)

# shared space
redis_params = {"host": "localhost", "port": 6379, "db": 0}
